import argparse
import numpy


class Matrix():

    def __init__(self, data: list):
        self.parse_data(data)

    def parse_data(self, data):
        dict_with_data_in_list = {"dimension": data[0], "Matrix": data[1:-1], "Name": data[-1]}
        if len(dict_with_data_in_list["dimension"]) != 2:
            raise ValueError("Matrix dimension must consist of 2 elements ")
        self.matrix_name = dict_with_data_in_list["Name"][0]
        self.rows = dict_with_data_in_list["dimension"][0]
        self.columns = dict_with_data_in_list["dimension"][1]
        self.matrix = dict_with_data_in_list["Matrix"]

    @property
    def matrix_name(self):
        return self.__matrix_name

    @matrix_name.setter
    def matrix_name(self, matrix_data):
        if not matrix_data[0].isalpha():
            raise TypeError("Matrix dimension must be integer greater than 0")
        self.__matrix_name = (str(matrix_data)).upper()

    @property
    def rows(self):
        return self.__rows

    @rows.setter
    def rows(self, rows_data):
        if not rows_data.isdigit():
            raise TypeError("Matrix dimension must be integer greater than 0")
        self.__rows = int(rows_data)

    @property
    def columns(self):
        return self.__columns

    @columns.setter
    def columns(self, columns_data):
        if not columns_data.isdigit():
            raise TypeError("Matrix dimension must be integer greater than 0")
        self.__columns = int(columns_data)

    @property
    def matrix(self):
        return self.__matrix

    @staticmethod
    def is_number(x):
        try:
            float(x)
            return True
        except:
            return False

    @matrix.setter
    def matrix(self, data_matrix):
        if self.__rows != len(data_matrix):
            raise ValueError("Uncorrected count of rows!!!")
        for i in range(len(data_matrix)):
            if len(data_matrix[i]) != self.__columns:
                raise ValueError("Uncorrected count of columns!!!")
        for i in range(self.__rows):
            for j in range(self.__columns):
                if self.is_number(data_matrix[i][j]):
                    if data_matrix[i][j].isdigit():
                        data_matrix[i][j] = int(data_matrix[i][j])
                    else:
                        data_matrix[i][j] = float(data_matrix[i][j])
                else:
                    raise TypeError("Elements on matrix must be digits!!!")
        self.__matrix = data_matrix

    def __add__(self, other):
        matrix_answer1 = list()
        matrix_answer = list()
        if self.__rows != other.__rows or self.__columns != other.__columns:
            raise ValueError("Dimension of Matrix must be same")
        for i in range(len(self.__matrix)):
            for j in range(len(self.__matrix[i])):
                if isinstance((self.__matrix[i][j] + other.__matrix[i][j]), float) and not (
                        self.__matrix[i][j] + other.__matrix[i][j]).is_integer():
                    matrix_answer1.append(round(self.__matrix[i][j] + other.__matrix[i][j], 5))
                else:
                    matrix_answer1.append(int(self.__matrix[i][j] + other.__matrix[i][j]))
            matrix_answer.append(matrix_answer1)
            matrix_answer1 = []
        print("Summed Matrix:", self.__matrix_name, "+", other.__matrix_name)
        print("Dimension:", self.__rows, "x", self.__columns)
        print("Matrix sum:")
        ans = ""
        for l in matrix_answer:
            x = " ".join(map(str, l))
            print(x, end="\n")

    def __sub__(self, other):
        matrix_answer1 = list()
        matrix_answer = list()
        if self.__rows != other.__rows or self.__columns != other.columns:
            raise ValueError("Dimension of Matrix must be same")
        for i in range(len(self.__matrix)):
            for j in range(len(self.__matrix[i])):
                if isinstance((self.__matrix[i][j] - other.__matrix[i][j]), float) and not (
                        self.__matrix[i][j] - other.__matrix[i][j]).is_integer():
                    matrix_answer1.append(round((self.__matrix[i][j] - other.__matrix[i][j]), 5))
                else:
                    matrix_answer1.append(int(self.__matrix[i][j] - other.__matrix[i][j]))
            matrix_answer.append(matrix_answer1)
            matrix_answer1 = []
        print("Subtracted Matrix:", self.__matrix_name, "-", other.__matrix_name)
        print("Dimension:", self.__rows, "x", self.__columns)
        print("Matrix subtraction:")
        ans = ""
        for l in matrix_answer:
            x = " ".join(map(str, l))
            print(x, end="\n")

    def __mul__(self, other):
        if self.__columns != other.__rows:
            raise ValueError("Columns of the first matrix must equal of rows of the second matrix")
        matrix_answer1 = list()
        matrix_answer = list()
        for i in range(self.__rows):
            for j in range(other.__columns):
                number = 0
                for l in range(self.__columns):
                    number += self.__matrix[i][l] * other.__matrix[l][j]
                if isinstance(number, float) and not number.is_integer():
                    matrix_answer1.append(round(number, 5))
                else:
                    matrix_answer1.append(int(number))
            matrix_answer.append(matrix_answer1)
            matrix_answer1 = []
        print("Multiplied Matrix:", self.__matrix_name, "*", other.__matrix_name)
        print("Dimension:", self.__rows, "x", other.__columns)
        print("Matrix multiple:")
        ans = ""
        for k in matrix_answer:
            x = " ".join(map(str, k))
            print(x, end="\n")

    def matrix_determinant(self):
        if self.__rows != self.__columns:
            raise IndexError("To count matrix determinant the matrix must be quadratic")
        print("Matrix", self.__matrix_name, "determinant:")
        determinant = numpy.linalg.det(self.__matrix)
        if isinstance(determinant, float) and not determinant.is_integer():
            print(round(determinant, 4))
        else:
            print(int(determinant))


print("'+' is a symbol of adding.")
print("'-' is a symbol of subtraction.")
print("'*' is a symbol of multiple.")
print("'~' is a symbol of matrix determinant.")

parser = argparse.ArgumentParser(description='Path to file in console')
parser.add_argument('-path', "--path_to_file")
args = parser.parse_args()
path = args.path_to_file
if path is None:
    print("Write path to file in which is your matrix:")
    path = input()
    if not isinstance(path, str):
        raise TypeError("Uncorrect path to file")
with open(path, "r") as f:
    data = f.read()
    data = [line.split() for line in data.split("\n")]
    data1 = list()
    data2 = list()
    for i in data:
        if len(i) == 1 and (i[0] == "-" or i[0] == "+" or i[0] == "*" or i[0] == "~"):
            symbol = i[0]
            break
        elif len(i) == 1 and not i[0].isnumeric():
            data1.append(i)
        else:
            data1.append(i)
    data2 = [i for i in data[len(data1) + 1:]]
if symbol == "~":
    A = Matrix(data1)
else:
    A = Matrix(data1)
    B = Matrix(data2)
if symbol == "-":
    A - B
if symbol == "+":
    A + B
if symbol == "*":
    A * B
if symbol == "~":
    A.matrix_determinant()



